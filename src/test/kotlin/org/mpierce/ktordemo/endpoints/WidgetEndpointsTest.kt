package org.mpierce.ktordemo.endpoints

import com.fasterxml.jackson.databind.JsonNode
import io.ktor.application.Application
import io.ktor.http.HttpMethod
import io.ktor.http.HttpStatusCode
import io.ktor.server.testing.handleRequest
import io.ktor.server.testing.setBody
import io.ktor.server.testing.withTestApplication
import org.apache.commons.configuration.EnvironmentConfiguration
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test
import org.mpierce.ktordemo.DbTestBase
import org.mpierce.ktordemo.KtorDemoConfig
import org.mpierce.ktordemo.jsonTestHelper
import org.skife.config.CommonsConfigSource
import org.skife.config.ConfigurationObjectFactory
import setupKtor

internal class WidgetEndpointsTest : DbTestBase() {
    private val appInit: Application.() -> Unit = {
        val config = ConfigurationObjectFactory(CommonsConfigSource(EnvironmentConfiguration()))
                .build(KtorDemoConfig::class.java)

        setupKtor(dsl, this, config)
    }

    @Test
    internal fun testGetWidgets() {
        withTestApplication(appInit) {
            with(handleRequest(HttpMethod.Get, "/widgets/all")) {
                assertEquals(HttpStatusCode.OK, response.status())
            }
        }
    }

    @Test
    internal fun testCreateWidget() {
        withTestApplication(appInit) {
            val widgetId = with(handleRequest(HttpMethod.Post, "/widgets") {
                setBody("""
                   {
                        "name": "foobar",
                        "buttons": [
                            {
                               "name": "foobars best button"
                            },
                            {
                                "name": "foobars worst button"
                            }
                        ]
                    }
                """)
            }) {
                assertEquals(HttpStatusCode.OK, response.status())
                val root = jsonTestHelper.reader.forType(JsonNode::class.java).readValue<JsonNode>(response.content!!)
                root.at("/id").intValue()
            }

            with(handleRequest(HttpMethod.Get, "/widgets/id/$widgetId")) {
                assertEquals(HttpStatusCode.OK, response.status())
                val root = jsonTestHelper.reader.forType(JsonNode::class.java).readValue<JsonNode>(response.content!!)

                jsonTestHelper.assertJsonEquals(""""foobar"""", root.at("/name"))
            }
        }
    }
}

