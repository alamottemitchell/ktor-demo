package org.mpierce.ktordemo

import buildObjectMapper
import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.ObjectReader
import com.fasterxml.jackson.databind.ObjectWriter
import com.zaxxer.hikari.HikariConfig
import com.zaxxer.hikari.HikariDataSource
import org.jooq.SQLDialect
import org.jooq.impl.DSL
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.BeforeEach
import org.mpierce.ktordemo.jooq.Tables

val jsonTestHelper = JsonTestHelper(buildObjectMapper())

abstract class DbTestBase {
    val hikariConfig = HikariConfig().apply {
        jdbcUrl = "jdbc:postgresql://localhost:25432/ktor-demo-test"
        username = "ktor-demo-test"
        password = "ktor-demo-test"
        // Tests run faster with smaller pool
        maximumPoolSize = 2
        minimumIdle = 1
    }

    private val dataSource = HikariDataSource(hikariConfig)
    internal val dsl = DSL.using(dataSource, SQLDialect.POSTGRES)


    @BeforeEach
    internal fun cleanTables() {
        dsl.transaction { c ->
            c.dsl().apply {
                batch(
                        deleteFrom(Tables.BUTTONS),
                        deleteFrom(Tables.WIDGETS)
                )
                        .execute()
            }
        }
    }

    @AfterEach
    internal fun closeDb() {
        dsl.close()
        dataSource.close()
    }
}

class JsonTestHelper(mapper: ObjectMapper) {

    internal val writer: ObjectWriter = mapper.writer()
    internal val reader: ObjectReader = mapper.reader()

    fun assertJsonEquals(expected: String, entity: Any) {
        val expectedNode = reader.forType(JsonNode::class.java).readValue<JsonNode>(expected)
        val actualNode = reader.forType(JsonNode::class.java).readValue<JsonNode>(writer.writeValueAsString(entity))

        Assertions.assertEquals(expectedNode, actualNode)
    }

    fun assertJsonStrEquals(expected: String, entity: String) {
        val expectedNode = reader.forType(JsonNode::class.java).readValue<JsonNode>(expected)
        val actualNode = reader.forType(JsonNode::class.java).readValue<JsonNode>(entity)

        Assertions.assertEquals(expectedNode, actualNode)
    }
}