CREATE TABLE buttons
(
  id         serial       NOT NULL PRIMARY KEY,
  widget_id  integer NOT NULL REFERENCES widgets,
  name       varchar(100) NOT NULL,
  created_at timestamp without time zone NOT NULL default now()
)
