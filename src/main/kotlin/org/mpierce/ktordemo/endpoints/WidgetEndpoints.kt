package org.mpierce.ktordemo.endpoints

import com.fasterxml.jackson.annotation.JsonProperty
import com.google.inject.Inject
import io.ktor.application.Application
import io.ktor.application.ApplicationCall
import io.ktor.application.call
import io.ktor.request.receive
import io.ktor.response.respond
import io.ktor.routing.get
import io.ktor.routing.post
import io.ktor.routing.routing
import kotlinx.coroutines.experimental.CoroutineDispatcher
import kotlinx.coroutines.experimental.async
import org.jooq.DSLContext
import org.mpierce.ktordemo.jooq.Keys
import org.mpierce.ktordemo.jooq.Tables.BUTTONS
import org.mpierce.ktordemo.jooq.Tables.WIDGETS
import org.mpierce.ktordemo.jooq.tables.records.ButtonsRecord
import org.mpierce.ktordemo.jooq.tables.records.WidgetsRecord
import java.time.Instant

/**
 * Instead of declaring endpoint logic inline, we can also do it with a class with DI by Guice.
 */
class WidgetEndpoints @Inject constructor(app: Application, val jooq: DSLContext, val ioDispatcher: CoroutineDispatcher) {
    init {
        app.routing {
            get("/widgets/id/{id}") {
                getWidget(call)
            }
            get("/widgets/all") {
                listWidgets(call)
            }

            post("/widgets") {
                createWidget(call)
            }
        }
    }

    private suspend fun createWidget(call: ApplicationCall) {
        val req = call.receive<WidgetPost>()

        var result = async(ioDispatcher) {
            jooq.transactionResult { configuration ->
                val widgetResult = configuration.dsl().insertInto(WIDGETS, WIDGETS.NAME)
                        .values(req.name)
                        .returning(WIDGETS.ID, WIDGETS.CREATED_AT)
                        .fetchOne()

                val widgetId = widgetResult.getValue(WIDGETS.ID)

                val buttons = req.buttons.map { button ->
                    configuration.dsl().newRecord(BUTTONS).apply {
                        this.name = button.name
                        this.widgetId = widgetId
                        store()
                    }

                }

                val buttonsResponse = buttons.map { ButtonResponse(it.name) }
                WidgetResponse(widgetId, req.name, widgetResult.getValue(WIDGETS.CREATED_AT).toInstant(), buttonsResponse)
            }
        }.await()

        call.respond(result)
    }

    private suspend fun listWidgets(call: ApplicationCall) {
        val widgets = async(ioDispatcher) {
            jooq.fetch(WIDGETS
                    .leftJoin(BUTTONS)
                    .on(WIDGETS.ID.eq(BUTTONS.WIDGET_ID)))
                    .map { r ->
                        Pair(
                                r.into(WIDGETS),
                                r.into(BUTTONS)
                        )
                    }
                    .groupBy { p ->
                        p.first.id
                    }.entries.map { entry ->
                val widget = entry.value.first().first
                val buttons = entry.value.map { it.second }
                WidgetResponse(widget, buttons)
            }
        }
        call.respond(widgets.await())
    }

    private suspend fun getWidget(call: ApplicationCall) {
        // Could also use a typed location to avoid the string-typing https://ktor.io/samples/locations.html
        val id = call.parameters["id"]!!.toInt()
        val r = async(ioDispatcher) {
            val pairs = jooq.select().from(WIDGETS)
                    .join(BUTTONS)
                    .onKey(Keys.BUTTONS__BUTTONS_WIDGET_ID_FKEY)
                    .where(WIDGETS.ID.eq(id))
                    .fetch()
                    .map { r ->
                        Pair(
                                r.into(WIDGETS),
                                r.into(BUTTONS)
                        )
                    }
            WidgetResponse(pairs.first().first, pairs.map { it.second })
        }.await()
        call.respond(r)
    }
}

/**
 * Deserialized from POSTed JSON.
 */
data class WidgetPost(@JsonProperty("name") val name: String, @JsonProperty("buttons") val buttons: List<ButtonPost>)

data class ButtonPost(@JsonProperty("name") val name: String)

/**
 * Serialized into a JSON response.
 */
data class WidgetResponse(@JsonProperty("id") val id: Int,
                          @JsonProperty("name") val name: String,
                          @JsonProperty("createdAt") val createdAt: Instant,
                          @JsonProperty("buttons") val buttons: List<ButtonResponse>) {

    constructor(widget: WidgetsRecord, buttons: List<ButtonsRecord>) : this(
            widget.id,
            widget.name,
            widget.createdAt.toInstant(),
            buttons.filter { it?.name != null }.map { ButtonResponse(it.name) }
    )

}


data class ButtonResponse(@JsonProperty("name") val name: String)

